import pytest
from pytest_mock import mocker

import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')
from authentication import auth
from mongo import mongoDb
from redisCache import MemCache
import main


@pytest.fixture
def app():
    app = main.create_app(True)
    yield app

@pytest.fixture
def client(app):
    return app.test_client()

@pytest.fixture
def appContext(app):
    return app.app_context()

@pytest.fixture
def authorized( mocker ):
    mocked_internal_func = mocker.patch.object(auth, 'check')
    mocked_internal_func.return_value = True

@pytest.fixture
def unauthorized( mocker ):
    mocked_internal_func = mocker.patch.object(auth, 'check')
    mocked_internal_func.return_value = False

    
@pytest.fixture    
def authorization_exception( mocker ):
    mocked_internal_func = mocker.patch.object(auth, 'check')
    mocked_internal_func.return_value = "Exception Error"
    
@pytest.fixture
def mongoDb_search_valid( mocker ):
    mocked_internal_func_search = mocker.patch.object(mongoDb, 'search')
    mocked_internal_func_search.return_value = { "uuid": "uuid", "data": "data" }
    
@pytest.fixture
def mongoDb_search_invalid( mocker ):
    mocked_internal_func = mocker.patch.object(mongoDb, 'search')
    mocked_internal_func.return_value = { "uuid": "", "data": "", "error": "error" }
    
@pytest.fixture
def mongoDb_insert( mocker ):
    mocked_internal_func = mocker.patch.object(mongoDb, 'insert')

@pytest.fixture
def redis_get( mocker ):
    mocked_internal_func = mocker.patch.object(MemCache, 'get')
    mocked_internal_func.return_value = { "data": "data" }
    
@pytest.fixture
def redis_put( mocker ):
    mocked_internal_func = mocker.patch.object(MemCache, 'put')

@pytest.fixture
def mongoDb_health( mocker ):
    mocked_internal_func = mocker.patch.object(mongoDb, 'health')
    mocked_internal_func.return_value = {
                    "server_status": "ok",
                    "version": "version",
                    "connection": True
               }
               
@pytest.fixture
def redis_health( mocker ):
    mocked_internal_func = mocker.patch.object(MemCache, 'health')
    mocked_internal_func.return_value = {
                    "server_version": "version",
                    "server_mode": "redis_mode",
                    "memory_size": "size"
               }