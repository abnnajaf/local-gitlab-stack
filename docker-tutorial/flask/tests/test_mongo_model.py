import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

import pytest
import json
import uuid

from mongo import model

valid_uuid = "123"
class TestModel():
    
    def test_constructor(self):
        _model = model(valid_uuid)
        assert _model.uuid == valid_uuid
        assert _model.data == None
        
    def test_serialize(self):
        _model = model(valid_uuid)
        _serializedModel = _model.serialize()
        assert isinstance( _serializedModel, dict )
        
    def test_save(self, appContext, mongoDb_insert):
        _model = model(valid_uuid)
        with appContext:
            _insertedID = _model.save()
        assert _insertedID != None
            
    def test_restore_with_invalid_uuid(self, appContext, mongoDb_search_invalid):
        _model = model("000")
        with appContext:
            result = _model.restore()
            assert isinstance(result, dict)
            assert result.get("error") != None
            
    def test_restore_with_valid_uuid(self, appContext, mongoDb_search_valid):
        _model = model(valid_uuid)
        with appContext:
            result = _model.restore()
            assert isinstance(result, dict)
            assert result.get("error") is None