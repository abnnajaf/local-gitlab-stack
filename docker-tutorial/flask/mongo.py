import pymongo
from pymongo import MongoClient
import datetime
import sys
from flask import current_app as app

class mongoDb():

    """
    DB stores model objects which contain result of each batch work
    """
    def __init__(self):
        # Use the app config to fetch DB params
        self.client = MongoClient( app.config.get("MONGO_URI") )
        self.db = self.client[ app.config.get("MONGO_DBNAME") ]
        self.coll = self.db[ app.config.get("COLLECTION") ]

    def search(self, uuid):
        try:
            return self.coll.find_one({"uuid": uuid}, {"_id":0}) # don't show ObjectID
        except pymongo.errors.PyMongoError as e:
            print( "Search over MongoDB failed:\n {}".format( e.message ) )
        return

    def insert(self, data):
        try:
            res = self.coll.insert_one(data)
            return res.inserted_id
        except pymongo.errors.PyMongoError as e:
            print( "Insert to MongoDB failed:\n {}".format( e.message ) )
        return

    def health(self):

        return {
                    "server_status": self.client.server_info().get("ok", 0),
                    "version": self.client.server_info().get("version"),
                    "connection": True if self.coll else False
               }

class model():

    def __init__(self, uuid, data=None, error=None):
        self.uuid = uuid
        self.data = data
        self.error = error

    def save(self):
        # Save the object in mongo and return the ObjectId
        mongo = mongoDb()
        return mongo.insert( self.serialize() )

    def restore(self):
        # TO DO: retrieve the object from mongo using uuid
        mongo = mongoDb()
        res = mongo.search( self.uuid )

        if res is None:
            res = self.serialize()
            res["error"] = "No execution with the given UUID found"
        return res

    def serialize(self):
        serial = dict()
        serial["uuid"] = self.uuid
        serial["data"] = self.data
        serial["error"] = self.error
        return serial