from main import create_app
import os

port = int(os.environ.get('PORT', 8080))

app = create_app(dev_config=True)

app.run(host='0.0.0.0', port=port)
