from flask import Flask, request, jsonify, Response
from mongo import model, mongoDb
import uuid
from authentication import auth
from redisCache import MemCache
import time
import json

def configure_app(app, dev_config):

    if dev_config is True:
        app.config.from_object('config.BaseConfig')
        app.logger.info("Database is {}".format(app.config.get('MONGO_URI')))

    else:
        app.config.from_object('config.ProductionConfig')

    return

def create_app(dev_config=False):

    # create and configure the app
    app = Flask(__name__)
    configure_app(app, dev_config)

    @app.route('/api/v1/')
    def hello_world():
        return "Hello World :)", 200

    @app.route('/api/v1/cache', methods=['POST'])
    def cacheData():

        _authenticated = auth().check( request.headers.get('secret') )
        res = dict()
        if _authenticated is False:
            res["error"] = 'Authentication Failed!'
            return jsonify(res), 401
        elif _authenticated is True:
            pass
        else:
            res["error"] = _authenticated
            return jsonify(res), 500
        
        data = request.get_json(silent=True)

        if data is None:
            res["error"] = "Invalid data!"
            return jsonify(res), 400
            
        cache = MemCache()

        _expiry = time.time() + 120

        data["expire_time"] = time.strftime("%H:%M:%S",time.localtime(_expiry))

        if request.method == 'POST':
            cache.put(data.get("username"), data, 120)
            return jsonify(data), 200

        return {}, 500

    @app.route('/api/v1/cache/<_key>')
    def getCachedData(_key):
    
        _authenticated = auth().check( request.headers.get('secret') )
        res = dict()
        if _authenticated is False:
            res["error"] = 'Authentication Failed!'
            return jsonify(res), 401
        elif _authenticated is True:
            pass
        else:
            res["error"] = _authenticated
            return jsonify(res), 500
            
        cache = MemCache()
        data = cache.get(_key)
        return jsonify(data), 200

    @app.route('/api/v1/persist', methods=['POST'])
    def persistData():

        _authenticated = auth().check( request.headers.get('secret') )
        res = dict()
        if _authenticated is False:
            res["error"] = 'Authentication Failed!'
            return jsonify(res), 401
        elif _authenticated is True:
            pass
        else:
            res["error"] = _authenticated
            return jsonify(res), 500
            
        data = request.get_json()

        prefix = time.time()

        if request.method == 'POST':
            with open('/tmp/file_' + str(prefix) + '.txt', 'w') as file:
                file.write(json.dumps(data))

            return {}, 200

        return {}, 500

    @app.route('/api/v1/get/<_uuid>')
    def getData(_uuid):

        _authenticated = auth().check( request.headers.get('secret') )
        res = dict()
        if _authenticated is False:
            res["error"] = 'Authentication Failed!'
            return jsonify(res), 401
        elif _authenticated is True:
            pass
        else:
            res["error"] = _authenticated
            return jsonify(res), 500

        # validate the given UUID
        is_valid_uuid = True
        _uuid_hex = None
        try:
            _uuid_hex = uuid.UUID(_uuid.replace("-", ""), version=4).hex
            is_valid_uuid = ( _uuid_hex == _uuid.replace("-", "") )
        except Exception as e:
            is_valid_uuid = False

        if is_valid_uuid is False:
            return jsonify( model(uuid=_uuid, error="Invalid UUID").serialize() ), 400

        stat = model( uuid = _uuid )
        return jsonify( stat.restore() ), 200

    @app.route('/api/v1/put', methods=['POST'])
    def putData():

        _authenticated = auth().check( request.headers.get('secret') )
        res = dict()
        if _authenticated is False:
            res["error"] = 'Authentication Failed!'
            return jsonify(res), 401
        elif _authenticated is True:
            pass
        else:
            res["error"] = _authenticated
            return jsonify(res), 500

        data = request.get_json(silent=True)

        if data is None:
            res["error"] = "Invalid data!"
            return jsonify(res), 400

        # return the request url
        _uuid = str(uuid.uuid4())
        res["executionUuid"] = _uuid

        stat = model( uuid = _uuid, data = data )
        stat.save()

        return jsonify(res), 202

    @app.route('/api/v1/health')
    def health():

        _mongoDb = mongoDb().health()
        _redis = MemCache().health()

        res = dict()
        res["MongoDB"] = _mongoDb
        res["Redis"] = _redis

        return jsonify(res), 200
        
    return app
