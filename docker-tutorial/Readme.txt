Pre:
- source /home/milad/Desktop/VirtualEnv/bin

Example 1. (port mapping)

- Run MongoDB in a docker container
- Run Redis in another docker container
- Run Flask locally and test it

Example 2. (networking)

- Run MongoDB in a container
- Run Redis in another docker container
- Run Flask in another container

Example 2. (docker compose)

- docker compose up
- ERROR: the MongoURI and RedisURI should be changed in the config.py file

