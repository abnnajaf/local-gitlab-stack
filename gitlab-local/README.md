### Run docker swarm

1. Setup docker compose 

```
https://docs.docker.com/compose/install/
```

2. To run Gitlab with a runner instances:

```
https://docs.gitlab.com/omnibus/docker/README.html#deploy-gitlab-in-a-docker-swarm
```

`docker-dompose.yml` file can be as following. It runs a cluster of gitlab-ce as a server and 2 runners each will be connected to the server as a `shell` executor, however, one will run docke containers (docker in docker) and one will execute jobs locally.

```
version: "3.6"
services:
  gitlab:
    image: gitlab/gitlab-ce:12.2.4-ce.0
    ports:
      - "2222:22"
      - "8080:80"
      - "4430:443"
    volumes:
      - /srv/gitlab/data:/var/opt/gitlab
      - /srv/gitlab/logs:/var/log/gitlab
      - /srv/gitlab/config:/etc/gitlab
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        gitlab_rails['lfs_enabled'] = true
    secrets:
      - gitlab_root_password

  gitlab-runner:
    image: gitlab/gitlab-runner:ubuntu-v12.2.0
    depends_on:
      - 'gitlab'
    deploy:
      mode: replicated
      replicas: 1
      
  gitlab-runner-dind:
    image: docker:dind
    privileged: true
    depends_on:
      - 'gitlab-server'
    deploy:
      mode: replicated
      replicas: 1

secrets:
  gitlab_root_password:
    file: ./root_password.txt
```

3. Use the following command to make sure everything goes well

```
docker ps

docker logs -f <container id>
```

### Prepare the Shell Runner instance

```
apt-get update

curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash

apt-get install --assume-yes python git-lfs python-pip

pip install virtualenv

virtualenv venv
```

### Prepare the dind Runner instance

```
# https://docs.gitlab.com/runner/install/linux-manually.html
# https://gitlab.com/gitlab-org/gitlab-runner/blob/master/dockerfiles/alpine/Dockerfile

adduser -D -S -h /home/gitlab-runner gitlab-runner

apk add --no-cache \
    bash \
    ca-certificates \
    git \
    openssl \
    tzdata \
    wget \
    curl
    
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

chmod +x /usr/local/bin/gitlab-runner

# I am not sure if necessary
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner 

ln -s /usr/bin/gitlab-runner /usr/bin/gitlab-ci-multi-runner

mkdir -p /etc/gitlab-runner/certs
chmod -R 700 /etc/gitlab-runner

# install docker machine
wget -nv https://github.com/docker/machine/releases/download/v0.16.0/docker-machine-Linux-x86_64 -O /usr/bin/docker-machine
chmod +x /usr/bin/docker-machine
docker-machine --version

# install dumb
wget -nv https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 -O /usr/bin/dumb-init
chmod +x /usr/bin/dumb-init
dumb-init --version

# install git-lfs
wget -nv https://github.com/git-lfs/git-lfs/releases/download/v2.8.0/git-lfs-linux-amd64-v2.8.0.tar.gz -O /tmp/git-lfs.tar.gz
mkdir /tmp/git-lfs
tar -xzf /tmp/git-lfs.tar.gz -C /tmp/git-lfs/
mv /tmp/git-lfs/git-lfs /usr/bin/git-lfs
rm -rf /tmp/git-lfs*
git-lfs install --skip-repo
git-lfs version

# install python and virtualenv

apk add --update python python-dev  py-pip
pip install virtualenv

```

### Configuring Gitlab and Runner

1. Login to the Gitlab using http://localhost:[port] and setup your inital password

2. Create a project and clone it locally to make sure everything works well

3. Open Gitlab dashboard http://localhost:[port]/admin/runners to find out the TOKEN required to setup a shared runner instance

4. Register and run the runners

```
# SHELL RUNNER

gitlab-runner register --non-interactive --url "http://gitlab-container-name"  --registration-token "TOKEN" \
  --executor "shell" \
  --description "shell-runner" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

gitlab-runner start

# DIND RUNNER

gitlab-runner register --non-interactive --url "http://gitlab-container-name"  --registration-token "TOKEN" \
  --executor "shell" \
  --description "dind runner" \
  --tag-list "dind" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

gitlab-runner run --working-directory=/home/gitlab-runner
```

### Executing a pipeline on the `dind` runner

The `dind` runner is able to pull and run docker containers inside itself.
So a `.gitlab-ci.yml` pipeline file can be as below to enable integration test. In this case, a python image will be spinned up to host the python code and a redis and mongodb containers will be also spinned up as services.
All the networking stuffs happen behind the scene.

```
image: python:latest

services:
  - redis:4.0.11-alpine
  - bitnami/mongodb:3.6.13-debian-9-r74

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r requirements.txt

test:
  script:
    - pytest tests/
    - pytest integration-tests/
```

You can run the python code inside the `dind` container as well. In this case, you need to install `python` and all the other necessary stuffs before on it.
```
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r requirements.txt
  - docker run --rm -tid -p 6379:6379 --name redis redis:4.0.11-alpine
  - docker run --rm -d -p 27017:27017 --name mongodb bitnami/mongodb:3.6.13-debian-9-r74

test:
  script:
    - pytest tests/
    - pytest integration-tests/

after_script:
  - docker stop $(docker ps -a -q) >> /dev/null 2>&1

```

### Executing a pipeline on the `shell` runner

To support integration test on the `shell` container you need to run the redis and mongodb containers along with the others by including the below lines in the `docker-compose.yml` file.

```
redis:
    image: redis:4.0.11-alpine
    ports:
      - "6379:6379"

  mongodb:
    image: bitnami/mongodb:3.6.13-debian-9-r74
    ports:
      - "27017:27017"
```

And having a `.gitlab-ci.yml` similar to below. You obviously need to install `python` and all necessary stuffs on it in advanced.
```
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - pip install -r requirements.txt

test:
  script:
    - pytest tests/
    - pytest integration-tests/
```