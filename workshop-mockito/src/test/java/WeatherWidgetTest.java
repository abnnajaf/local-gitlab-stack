import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WeatherWidgetTest {

    private WeatherWidget weatherWidget;
    private Position position;
    private Weather weather;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        weatherWidget = new WeatherWidget();
        position = new Position(1,2);
        weather = new Weather(20, 90);
    }

    @Test
    public void install() {
        weatherWidget.install(position);

        Assert.assertEquals(true, weatherWidget.isInstalled());
        Assert.assertEquals(position, weatherWidget.getPosition());
    }

    @Test
    public void update() {
        weatherWidget.install(position);
        weatherWidget.setCity("MHD");
        weatherWidget.update("MHD", weather);

        Assert.assertEquals(weather, weatherWidget.getWeather());
    }

    @Test
    public void update_Irrelevant() {
        weatherWidget.install(position);
        weatherWidget.setCity("MHD");
        weatherWidget.update("THR", weather);

        Assert.assertEquals(null, weatherWidget.getWeather());
    }

    @Test
    public void uninstall() {
        weatherWidget.install(position);
        weatherWidget.uninstall();

        Assert.assertEquals(-1, weatherWidget.getPosition().getX());
        Assert.assertEquals(-1, weatherWidget.getPosition().getY());
    }

    @Test
    public void updatePosition() {
        weatherWidget.install(position);
        weatherWidget.updatePosition(10,10);

        Assert.assertEquals(10, weatherWidget.getPosition().getX());
        Assert.assertEquals(10, weatherWidget.getPosition().getY());
    }
}
