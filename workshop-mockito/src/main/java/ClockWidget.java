public class ClockWidget implements Widget {
    private long time;
    private boolean installed = false;
    private final boolean movable = true;
    private Position positionOnScreen;

    public ClockWidget() { }

    public void install(Position positionOnScreen) {
        installed = true;
        this.positionOnScreen = positionOnScreen;
        showTime();
    }

    public boolean isInstalled() {
        return installed;
    }

    public boolean isMovable() {
        return movable;
    }

    public void uninstall() {
        this.positionOnScreen.update(-1, -1);
    }

    public Position getPosition() {
        return positionOnScreen;
    }

    public Position updatePosition(int x, int y) {
        positionOnScreen.update(x, y);
        return  positionOnScreen;
    }

    public void showTime() {
        System.out.println(System.currentTimeMillis());
    }
}
