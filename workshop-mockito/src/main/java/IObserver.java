public interface IObserver {
    public void update(String city, Weather weather);
}