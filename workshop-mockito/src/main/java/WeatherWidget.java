public class WeatherWidget implements IObserver, Widget {
    private final boolean movable = true;
    private IPublisher publisher;
    private String city;
    private Weather weather;
    private boolean installed = false;
    private Position positionOnScreen;

    public WeatherWidget() {
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Weather getWeather() {
        return weather;
    }

    public void install(Position positionOnScreen) {
        install(Publisher.getInstance(), positionOnScreen);
    }

    public void install(IPublisher publisher, Position positionOnScreen) {
        this.publisher = publisher;
        this.publisher.attach(this);
        installed = true;
        this.positionOnScreen = positionOnScreen;
    }

    public void update(String city, Weather weather) {
        if (this.city.equalsIgnoreCase(city)) {
            this.weather = weather;
            this.showData();
        }
    }

    public boolean isInstalled() {
        return installed;
    }

    public boolean isMovable() {
        return movable;
    }

    public void uninstall() {
        this.publisher.detach(this);
        this.positionOnScreen.update(-1, -1);
    }

    public Position getPosition() {
        return positionOnScreen;
    }

    public Position updatePosition(int x, int y) {
        positionOnScreen.update(x, y);
        return positionOnScreen;
    }

    public void showData() {
        System.out.println(String.format("City: %s , Weather Forecast: %s", this.city, this.weather.toString()));
    }
}
