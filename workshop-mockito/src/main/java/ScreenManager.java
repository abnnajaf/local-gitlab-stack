import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ScreenManager {

    private int threadsCnt = 0;
    private List<Widget> widgetList = new ArrayList<Widget>();

    public boolean hasSpace(Position position) {
        if (position.getX() > 10 || position.getY() > 20)
            return false;

        return true;
    }

    public void assignThread(Widget widget) throws IOException{
        if (this.threadsCnt > 3) {
            throw new IOException();
        } else {
            widgetList.add(widget);
            // some operation
        }
    }

    public void dischargeThread(Widget widget) throws Exception{
        if (!widgetList.contains(widget)) {
            throw new Exception();
        } else {
            widgetList.remove(widget);
            // some operation
        }
    }
}
