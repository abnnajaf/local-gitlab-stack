import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Screen {

    private ScreenManager screenManager;
    private WidgetFactory widgetFactory;

    public Screen(ScreenManager screenManager, WidgetFactory widgetFactory) {
        this.screenManager = screenManager;
        this.widgetFactory = widgetFactory;
    }

    private List<Widget> widgets = new ArrayList<Widget>();

    public Widget installWidget(String widgetType, Position position) throws IOException {
        Widget widget = widgetFactory.getWidget(widgetType);
        widget.install(position);

        if (screenManager.hasSpace(position))
            screenManager.assignThread(widget);

        widgets.add(widget);
        return widget;
    }

    public void relocateWidget(Widget widget, Position position) {
        if(widget.isMovable() && screenManager.hasSpace(position))
            widget.updatePosition(position.getX(), position.getY());
    }

    public void uninstallWidget(Widget widget) throws Exception {
        if(widget.isInstalled()) {
            screenManager.dischargeThread(widget);
            widget.uninstall();
            widgets.remove(widget);
        }
    }

    public void cleanScreen() throws Exception {
        for(Widget widget : widgets) {
            uninstallWidget(widget);
        }
    }

    public List<Widget> getWidgets() {
        return widgets;
    }
}
