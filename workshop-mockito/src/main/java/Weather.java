public class Weather {
    private int temperature;
    private int pressure;

    public Weather(int temperature, int pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
    }

    @Override
    public String toString() {
        return String.format("Temperature: %d, Pressure: %d", temperature, pressure);
    }
}
