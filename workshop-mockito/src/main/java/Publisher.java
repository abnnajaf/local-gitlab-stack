import java.io.IOException;
import java.util.*;

import com.sun.scenario.effect.impl.sw.java.JSWBrightpassPeer;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpEntity;

public class Publisher implements IPublisher{

    private static final Logger logger = new Logger();

    private List<WeatherWidget> observersList;
    private String city;
    private Weather weather;
    private Timer timer;
    private HttpConnection httpConnection;

    static public Publisher publisher = null;

    public static Publisher getInstance() {
        if (publisher == null) {
            publisher = new Publisher();
        }
        return publisher;
    }

    private Publisher() {
        observersList = new ArrayList<WeatherWidget>();
        schedule(60);
    }

    public void setHttpConnection(HttpConnection httpConnection) {
        this.httpConnection = httpConnection;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public Timer getTimer() {
        if (this.timer == null)
            this.timer = new Timer();

        return timer;
    }

    public HttpConnection getHttpConnection() {
        if (this.httpConnection == null) {
            this.httpConnection = new HttpConnection();
        }
        return this.httpConnection;
    }

    private void schedule(long i) {
        getTimer().schedule(new TimerTask() {
            @Override
            public void run() {
                timedStart();
            }
        }, i);
    }

    void timedStart() {
        try {
            poll();
        } catch (Throwable t) {
            logger.log(t);
        }
    }

    public void attach(WeatherWidget o) { observersList.add(o); }

    public void detach(WeatherWidget o) {
        observersList.remove(o);
        if (observersList.size() == 0)
            timer.cancel();
    }

    public void publish() {
        for (WeatherWidget o : observersList)
            o.update(city, weather);
    }

    public void poll(){
        String forecastResponse = getHttpConnection().GET("https://forecast.com/");
        setWeatherForecast(forecastResponse);
    }

    public void setWeatherForecast(String forecast) {
        this.city = forecast.substring(0, forecast.indexOf(","));
        forecast = forecast.substring(forecast.indexOf(",")+1);

        int temperature = Integer.parseInt(forecast.substring(0, forecast.indexOf(",")));
        forecast = forecast.substring(forecast.indexOf(",")+1);

        int pressure = Integer.parseInt(forecast);

        this.weather = new Weather(temperature, pressure);
        publish();
    }
}
